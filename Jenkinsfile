def mvnCmd = "mvn -s artifact_setting.xml" 
def sonarProjectKey = "${JOB_NAME}_${BUILD_NUMBER}"
def artifactoryUrl = "http://ec2-52-53-39-8.us-west-1.compute.amazonaws.com:8081/artifactory/"
def jbossUrl = "ec2-13-52-50-90.us-west-1.compute.amazonaws.com:9990"
def buildname = "java-service2-build"
def buildnumber = "${BUILD_NUMBER}"

pipeline {
  agent {
      label 'maven-with-tools'
  }
  
  environment {
    M2_HOME = '/opt/rh/rh-maven35/root/usr/share/maven/'
  }

  stages {

    stage('Pull Code') {
      steps {
        git branch: 'master', url: 'https://gitlab.com/poc-ktb/java-service2.git'
      }
    }

    stage('Build App'){
      steps {
        sh "${mvnCmd} clean package -DskipTests=true" 
        input(ok: "Start", message: "End Stage Build App ...Pause...")
      }
    }

    stage('Unit Test'){
      steps {
        sh "${mvnCmd} test"
        input(ok: "Start", message: "End Stage Unit Test ...Pause...")
      }
    }

    stage('Scan Code'){
      steps {
        sh "echo sonar-scanner ..."
        withCredentials([usernamePassword(credentialsId: 'sonarqube-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
          withSonarQubeEnv('sonar-aws') {
            script {
              sonarProjectKey = sonarProjectKey.replace("/","_")
              sh "${mvnCmd} sonar:sonar -Dsonar.login=$USERNAME -Dsonar.password=$PASSWORD -Dsonar.projectKey=${sonarProjectKey} -Dsonar.projectName=${sonarProjectKey}"
            }
          }
        }
        sleep(10)
        waitForQualityGate abortPipeline: true      
        input(ok: "Start", message: "End Stage Scan Code ...Pause...")  
      }
    }
    
    // stage('Push to Artifactory') {
    //   steps{
    //     withCredentials([usernamePassword(credentialsId: 'jfrog-artifactory-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    //       script {
    //         sh "${mvnCmd} deploy -DskipTests=true -DaltDeploymentRepository=central::default::${artifactoryUrl}libs-release"
    //       }
    //     }
    //     input(ok: "Start", message: "End Stage Push to Artifactory ...Pause...")  
    //   }
    // }

    stage ('Push and Scan Artifactory') {
       steps {
         withCredentials([usernamePassword(credentialsId: 'jfrog-artifactory-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
           script {
              sh "jfrog rt c jfrog-aws --url=$artifactoryUrl --user=$USERNAME --password=$PASSWORD"
              sh "jfrog rt mvn 'clean install -DskipTests=true -f pom.xml' configuration.yml --build-name=${buildname} --build-number=${buildnumber}"
              //sh "jfrog rt bce ${buildname} ${buildnumber}"
              sh "jfrog rt bp ${buildname} ${buildnumber}"
              sleep(10)
              sh "jfrog rt bs ${buildname} ${buildnumber}"
           }
         }
       }
     }

    stage('Deploy Approval') {
      steps {
        timeout(10) {
          input(ok: "Deploy", message: "Should we deploy to Production?")
        }
      }
    }

    stage('Deploy War') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'jboss-aws', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
          sh "/opt/jboss/bin/jboss-cli.sh --connect --controller=${jbossUrl} --user=${USERNAME} --password=${PASSWORD} --command='deploy target/java-service2.war --force' "
        }
      }
    }
  }
}

// Convenience Functions to read variables from the pom.xml
// Do not change anything below this line.
def getVersionFromPom(pom) {
  def matcher = readFile(pom) =~ '<version>(.+)</version>'
  matcher ? matcher[0][1] : null
}
def getGroupIdFromPom(pom) {
  def matcher = readFile(pom) =~ '<groupId>(.+)</groupId>'
  matcher ? matcher[0][1] : null
}
def getArtifactIdFromPom(pom) {
  def matcher = readFile(pom) =~ '<artifactId>(.+)</artifactId>'
  matcher ? matcher[0][1] : null
}
