#!/bin/bash
# Setup Jenkins Project
echo "apiVersion: v1
items:
- kind: "BuildConfig"
  apiVersion: "v1"
  metadata:
    annotations:
      openshift.io/generated-by: OpenShiftNewBuild
    creationTimestamp: null
    labels:
      build: java-service2-pipeline
    name: java-service2-pipeline
  spec:
    source:
      type: "Git"
      git:
        uri: https://gitlab.com/poc-ktb/java-service2.git
    strategy:
      type: "JenkinsPipeline"
      jenkinsPipelineStrategy:
        env:
        - name: DEV_PROJECT
          value: dev
        - name: CICD_PROJECT
          value: ci-cd
        jenkinsfilePath: Jenkinsfile
    triggers:    
    - type: ConfigChange    
kind: List
metadata: []" | oc create -f - 
oc set triggers bc/java-service2-pipeline --from-gitlab=true